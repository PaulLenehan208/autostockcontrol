package AutoStockControl;


public class Autos {

	private int price;
	private String model;
	private String colour;
	private int engine_cc;
	private String transmission;
	private String fuel_type;
	private int co2_emissions;
	private static int numberOfCars;
	private static int total;


	public Autos (int price, String model, String colour, int engine_cc, String transmission, String fuel_type, int co2_emissions) {
		this.price= price;
		this.model = model;
		this.colour = colour;
		this.engine_cc = engine_cc;
		this.transmission = transmission;
		this.fuel_type = fuel_type;
		this.co2_emissions = co2_emissions;
		numberOfCars++;
		total = total + price;

	}
	public int getC02_emissions() {
		return co2_emissions;
	}
	public void setC02_emissions(int c02_emissions) {
		this.co2_emissions = c02_emissions;
	}
	public static int getTotal() {
		return total;

	}
	public static void setTotal(int total) {
		Autos.total = total;
	}

	public static int getNumberOfCars() {

		return numberOfCars;
	}

	public static void setNumberOfCars(int numberOfCars) {
		Autos.numberOfCars = numberOfCars;
	}

	@Override
	public String toString() {
		return "Autos [price=" + price + ", model=" + model  + ", colour=" + colour + ", engine_cc=" + engine_cc  + ", transmission=" + transmission + ", fuel_type=" + fuel_type + ", co2_emissions=" + co2_emissions+"]";

	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public int getEngine_cc() {
		return engine_cc;
	}

	public void setEngine_cc(int engine_cc) {
		this.engine_cc = engine_cc;
	}

	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	public String getFuel_type() {
		return fuel_type;
	}

	public void setFuel_type(String fuel_type) {
		this.fuel_type = fuel_type;
	}
	public char getTaxBand(int co2_emissions) {
		char taxBand = ' ';

		if (co2_emissions<= 120) {
			taxBand = 'A';
		} else if(co2_emissions <= 140) {
			taxBand = 'B';
		} else if(co2_emissions <= 155) {
			taxBand = 'C';
		}
		else if(co2_emissions<= 170) {
			taxBand = 'D';
		}
		else if(co2_emissions <= 190) {
			taxBand = 'E';
		}
		else if(co2_emissions <= 225) {
			taxBand = 'F';
		}
		else {
			taxBand = 'G';
		}
		return taxBand;
	}
}

