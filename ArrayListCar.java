
package AutoStockControl;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ArrayListCar {
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		final int ADD = 1, UPDATE = 2, VIEW_A_CAR = 3, DELETE_A_CAR = 4, DELETE_ALL_CARS = 5, SEARCH = 6,
				COUNT_NUMBER_OF_CARS_IN_STOCK = 7, TOTAL_VALUE_OF_STOCK = 8, TAX_BAND_CALCULATOR = 9, EXIT = 10;
		String userContinue = "y";

		ArrayList<Autos> car = new ArrayList<Autos>();
		
		System.out.println("Ford Dealership");
		while (userContinue.equalsIgnoreCase("y")) {
			switch (userChoice()) {
			case ADD:
				add(car);
				break;
			case UPDATE:
				update(car);
				break;
			case VIEW_A_CAR:
				view(car);
				break;
			case DELETE_A_CAR:
				deleteAutos(car);
				break;
			case DELETE_ALL_CARS:
				deleteAllAutos(car);
				break;
			case SEARCH:
				findAutosByModel(car);
				break;
			case COUNT_NUMBER_OF_CARS_IN_STOCK:
				countCarsInStock(car);
				break;
			case TOTAL_VALUE_OF_STOCK:
				calculateValueOfStock(car);
				break;
			case TAX_BAND_CALCULATOR:
				calculateTaxBand(car);
				break;
			case EXIT:
				userContinue = "n";
				break;
			default:
				System.out.println("Unknown entry : ");
				break;
			}
		}
	}

	public static int userChoice() {

		System.out.println("What would you like to do ?");
		System.out.println("1.  Add a car.\r");
		System.out.println("2.  Update a car.\r");
		System.out.println("3.  View a car.\r");
		System.out.println("4.  Delete a car.\r");
		System.out.println("5.  Delete all cars.\r");
		System.out.println("6.  Search.\r");
		System.out.println("7.  Count number of cars in stock.\r");
		System.out.println("8.  Calculate the total value of stock.\r");
		System.out.println("9.  Find tax band.\r");
		System.out.println("10. Exit.\r");

		// Catching Exception when an unexpected variable type is entered by the user
		try {
			return sc.nextInt();
		} catch (Exception e) {
			System.out.println("Invalid Entry! Program exit. Please re-run and enter a number between 1 & 9 ");
		} finally {
		}
		return 10;

	}

	public static void add(ArrayList<Autos> car) {
		// Get the car's model

		System.out.print("Enter the model --> ");
		System.out.print("Fiesta, Focus, Puma, Mondeo, Kuga, Mustang or Ranger?--> ");
		String model = sc.next();

		if (!(model.equalsIgnoreCase("fiesta") || model.equalsIgnoreCase("focus") || model.equalsIgnoreCase("mondeo")
				|| model.equalsIgnoreCase("puma") || model.equalsIgnoreCase("kuga") || model.equalsIgnoreCase("mustang")
				|| model.equalsIgnoreCase("ranger"))) {
			System.out.println("Invalid model!");

			return;
		} else {

		}
		System.out.print("Enter the car's price --> ");

		// Catching Exception when an unexpected variable type is entered by the user
		int price = 1000;
		try {
			price = sc.nextInt();

		} catch (InputMismatchException e) {
			{
			}
			sc.next();
			System.out.println("Invalid value. You must enter a number");
			Scanner sc = new Scanner(System.in);
			System.out.print("Enter the car's price --> ");
			price = sc.nextInt();
		}
		System.out.print("Enter the car's colour --> ");
		String colour = sc.next();
		System.out.print("Enter the car's engine cc  --> ");

		int engine_cc = 1000;

		// Catching Exception when an unexpected variable type is entered by the user
		try {
			engine_cc = sc.nextInt();

		} catch (InputMismatchException e) {
			{
			}
			sc.next();
			System.out.println("Invalid value. You must enter a number");
			Scanner sc = new Scanner(System.in);
			System.out.print("Enter the car's engine cc --> ");
			engine_cc = sc.nextInt();

		}

		System.out.print("Enter the car's transmission type --> ");
		System.out.print("Manual or Auto? --> ");
		String transmission = sc.next();
		if (!(transmission.equalsIgnoreCase("manual") || transmission.equalsIgnoreCase("auto") || transmission.equalsIgnoreCase("a") || transmission.equalsIgnoreCase("m"))) {
			System.out.println("Invalid transmission type");
			return;
		} else {

			System.out.print("Enter the car's fuel type --> ");
			System.out.print("Petrol, Diesel, Hybrid or Electric?--> ");
			String fuel_type = sc.next();
			if (!(fuel_type.equalsIgnoreCase("petrol") || fuel_type.equalsIgnoreCase("diesel")
					|| fuel_type.equalsIgnoreCase("hybrid") || fuel_type.equalsIgnoreCase("electric") || fuel_type.equalsIgnoreCase("p") || fuel_type.equalsIgnoreCase("d") || fuel_type.equalsIgnoreCase("h")|| fuel_type.equalsIgnoreCase("e"))) {
				System.out.println("Invalid fuel type");
				return;
			} else {

				int co2_emissions = 0;
				System.out.print("Enter the car's co2_emissions  --> ");

				// Catching Exception when an unexpected variable type is entered by the user
				try {
					co2_emissions = sc.nextInt();

				} catch (InputMismatchException e) {
					{
					}
					sc.next();
					System.out.println("Invalid value. You must enter a number");

					Scanner sc = new Scanner(System.in);
					System.out.println("Enter the car's co2_emissions --> ");
					co2_emissions = sc.nextInt();
				}
				Autos autos = new Autos(price, model, colour, engine_cc, transmission, fuel_type, co2_emissions);
				car.add(autos);
				System.out.println("<Record added>");
				return;

			}
		}
	}

	public static int autosThereAlready(String model, ArrayList<Autos> car) {
		int index = -1;
		for (int i = 0; i < car.size(); i++) {
			Autos autos = car.get(i);
			if (autos.getModel().equalsIgnoreCase(model)) {
				index = i;
				break;
			}
		}
		return index;
	}

	public static void update(ArrayList<Autos> car) {
		System.out.println("Update record details as appropriate ");
		System.out.println("Enter the car model you wish to change --> ");
		String model = sc.next();

		// If the autos model is not in AL then add autos
		int index = autosThereAlready(model, car);
		if (index == -1) { // not found
			System.out.println(model + " model is not in stock...");
		} else {
			Autos autos = car.get(index);
			System.out.print("Enter the model --> ");
			autos.setModel(sc.next());

			System.out.print("Enter the price --> ");

			try {
				autos.setPrice(sc.nextInt());

			} catch (InputMismatchException e) {
				{
				}
				sc.next();
				System.out.println("Invalid value. You must enter a number");
				Scanner sc = new Scanner(System.in);
				System.out.print("Enter the car's price --> ");
				autos.setPrice(sc.nextInt());
			}

			System.out.print("Enter the colour --> ");
			autos.setColour(sc.next());
			System.out.print("Enter the engine cc --> ");
			try {
				autos.setEngine_cc(sc.nextInt());

			} catch (InputMismatchException e) {
				{
				}
				sc.next();
				System.out.println("Invalid value. You must enter a number");
				Scanner sc = new Scanner(System.in);
				System.out.print("Enter the car's engine cc --> ");
				autos.setEngine_cc(sc.nextInt());

			}

			System.out.print("Enter the transmission --> ");
			autos.setTransmission(sc.next());
			System.out.print("Enter fuel type --> ");
			autos.setFuel_type(sc.next());
			System.out.print("Enter the car's co2_emissions  --> ");
			autos.setC02_emissions(sc.nextInt());
			System.out.println("<Record updated>");
			System.out.println("The updated car list : " + car);
		}
	}

	public static void deleteAutos(ArrayList<Autos> car) {
		System.out.println("The car list before : " + car);

		System.out.print("Enter the car model you wish to delete --> ");
		String model = sc.next();
		int index = autosThereAlready(model, car);
		if (index == -1) { // not found
			System.out.println("Could not find that car!");
		} else {
			car.remove(index);
		}
		System.out.println("Car deleted: The car list after : " + car);
	}

	public static void deleteAllAutos(ArrayList<Autos> car) {
		car.clear();
		System.out.println("All cars deleted");
	}

	public static void findAutosByModel(ArrayList<Autos> car) {

		System.out.print("Enter the car model --> ");
		String model = sc.next();
		boolean autosFound = false;
		for (Autos autos : car) {
			if (autos.getModel().equalsIgnoreCase(model)) {
				autosFound = true;
				System.out.println(autos);
				break;
			}
		}
		if (autosFound) {
			System.out.println("Found car...");

		} else {
			System.out.println("Could not locate the car!");
		}
	}

	public static void view(ArrayList<Autos> car) {
		for (Autos autos : car)
			System.out.println(autos);
	}

	public static void calculateTaxBand(ArrayList<Autos> car) {
		
		System.out.println("Enter car model to return tax band --> ");
		String model = sc.next();

		// If the autos model is not in AL then add autos
		int index = autosThereAlready(model, car);
		if (index == -1) { // not found
			System.out.println("There is no " + model + " currently on the system. Please add a " + model+ " to the system first, to calculate the tax band.");
		} else {
			Autos autos = car.get(index);
			System.out.print("The tax band for the " + model + " is:  Group " + autos.getTaxBand(autos.getC02_emissions()));
			System.out.println();
		}		
	}

	public static void countCarsInStock(ArrayList<Autos> car) {
		System.out.println("The number of cars currently in stock is: ");
		System.out.println(Autos.getNumberOfCars());
		System.out.println();

	}

	public static void calculateValueOfStock(ArrayList<Autos> car) {
		int total = (int) Autos.getTotal();
		System.out.println("The total value of cars currently in stock is: " + total);
	}
}
