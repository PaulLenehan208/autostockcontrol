package AutoStockControl;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import AutoStockControl.Autos;

class AutosTest {

	Autos autos;

	@BeforeEach
	public void setUp() {
		autos = new Autos(20000, "Focus", "black", 1600, "manual", "diesel", 121);
	}

	@Test
	void testEmployeeConstructed() {
		assertEquals(20000,autos.getPrice());
		assertEquals("Focus",autos.getModel());
		assertEquals("black",autos.getColour());
		assertEquals(1600,autos.getEngine_cc());
		assertEquals("manual",autos.getTransmission());
		assertEquals("diesel",autos.getFuel_type());
		assertEquals(121,autos.getC02_emissions());
	}

	@Test
	void testC02_emissionsChanged() {
		autos.setC02_emissions(145);
		assertEquals(145,autos.getC02_emissions());

	}

	@Test
	void testTotalSales() {
		autos.setTotal(0);
		autos = new Autos(20000, "Fiesta", "black", 2000, "manual", "diesel", 141);
		autos = new Autos(25000, "Focus", "black", 2000, "manual", "diesel", 141);
		autos = new Autos(30000, "Mondeo", "black", 2000, "manual", "diesel", 141);
		autos = new Autos(25000, "Puma", "black", 2000, "manual", "diesel", 141);
		autos = new Autos(40000, "Kuga", "black", 2000, "manual", "diesel", 141);
		autos = new Autos(50000, "Ranger", "black", 2000, "manual", "diesel", 141);
		autos = new Autos(60000, "Mustang", "black", 2000, "manual", "diesel", 141);
		assertEquals(250000,autos.getTotal());
	}
	@Test
	void testTotalSalesChanged() {
		autos.setTotal(100000);
		assertEquals(100000,autos.getTotal());
	}
	@Test
	void testNumberOfCarsChanged() {
		autos.setNumberOfCars(12);
		assertEquals(12,autos.getNumberOfCars());
	}

	@Test
	void testToString() {
		String result = "Autos [price=" + autos.getPrice() + ", model=" + autos.getModel()  + ", colour=" + autos.getColour() + ", engine_cc="
				+ autos.getEngine_cc()  + ", transmission=" + autos.getTransmission() + ", fuel_type=" + autos.getFuel_type() + ", co2_emissions=" + autos.getC02_emissions() +"]";
		assertEquals(autos.toString(), result);
	}

	@Test
	void testPriceChanged() {
		autos.setPrice(21000);
		assertEquals(21000,autos.getPrice());
	}

	@Test
	void testModelChanged() {
		autos.setModel("focus sport");
		assertEquals("focus sport",autos.getModel());
	}

	@Test
	void testColourChanged() {
		autos.setColour("white");
		assertEquals("white",autos.getColour());
	}

	@Test
	void testEngine_ccChanged() {
		autos.setEngine_cc(1800);
		assertEquals(1800,autos.getEngine_cc());
	}

	@Test
	void testTransmissionChanged() {
		autos.setTransmission("automatic");
		assertEquals("automatic",autos.getTransmission());	
	}

	@Test
	void testFuel_typeChanged() {
		autos.setFuel_type("hybrid");
		assertEquals("hybrid",autos.getFuel_type());	

	}

	@ParameterizedTest(name = "CO2 emissions of {0} is classes as Band {1}") 
	@CsvSource({"226,G","225,F","224,F","192,F","191,F","190,E","189,E","172,E","171,E","170,D","169,D","157,D","156,D","155,C","154,C","142,C","141,C","140,B","139,B","122,B","121,B","120,A","119,A","1,A","0,A",} )
	void testTaxbandEmissions(int co2_emissions, char taxBand) {autos.setC02_emissions(co2_emissions);
		assertEquals(taxBand,autos.getTaxBand(autos.getC02_emissions()));

	}	
	
}
